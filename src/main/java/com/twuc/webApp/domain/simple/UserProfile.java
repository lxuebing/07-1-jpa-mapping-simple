package com.twuc.webApp.domain.simple;

// TODO:
//
// 请创建一个 UserProfile entity。其对应的数据库表的 schema 必须满足如下 SQL 的要求：
//
// table: user_profile
// +────────────────+──────────────+───────────────────────────────+
// | column         | description  | additional                    |
// +────────────────+──────────────+───────────────────────────────+
// | id             | bigint       | auto_increment, primary key   |
// | name           | varchar(64)  | not null                      |
// | year_of_birth  | smallint     | not null                      |
// +────────────────+──────────────+───────────────────────────────+
//
//  请补全如下的代码，你可以新建各种方法、构造器，添加各种 annotation。但是添加的东西越少越好。
//
// <--start-

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class UserProfile {
    @Id
    private Long id;

    public UserProfile() {
    }

    public static UserProfile of(String name, short yearOfBirth) {
        return new UserProfile(name, yearOfBirth);
    }

    private UserProfile(String name, Short yearOfBirth) {
        // 请实现该构造函数
    }

    public Long getId() {
        // 请实现该方法
        return null;
    }

    public String getName() {
        // 请实现该方法
        return null;
    }

    public Short getYearOfBirth() {
        // 请实现该方法
        return null;
    }
}
// --end-->