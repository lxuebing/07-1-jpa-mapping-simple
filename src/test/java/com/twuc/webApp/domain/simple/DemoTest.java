package com.twuc.webApp.domain.simple;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.annotation.DirtiesContext;

import javax.persistence.EntityManager;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DataJpaTest
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
public class DemoTest {

    @Autowired
    PersonRepository personRepository;

    @Autowired
    EntityManager em;

    @Test
    void should_text_api_person() {
        assertTrue(true);
    }

    @Test
    void should_sava_a_person() {
        Person xuebing = new Person(1L, "xuebing", "Li");
        personRepository.save(xuebing);

        em.flush();

        Optional<Person> personId = personRepository.findById(1L);
        assertThat(personId.isPresent()).isTrue();
    }
}
